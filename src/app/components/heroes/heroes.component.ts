import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HeroesService, Heroe} from '../../servicios/heroes.service';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  
  public heroes:Heroe[] = [];

  constructor( 
    private _heroesService: HeroesService,
    private _router: Router
  ) {
    
   }

  ngOnInit() {

    this.heroes = this._heroesService.getHeroes();

    // console.log(this.heroes);

  }

  verHeroe(id:number){
    this._router.navigate(['/heroe',id]);
  }

}

