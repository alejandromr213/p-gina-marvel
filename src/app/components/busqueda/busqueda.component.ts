import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {HeroesService} from '../../servicios/heroes.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {
busqueda:any;
heroes:any[]=[];

  constructor(
    private activateRoute:ActivatedRoute,
    private _router:Router,
    private _heroesService:HeroesService
  ) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params=>{
      this.busqueda=(params['texto']);
      this.heroes=this._heroesService.buscarHeroes(params['texto']);
      // console.log(this.heroes);
    });
  }


  verHeroe(id:number){
    this._router.navigate(['/busqueda-Heroe',id]);
  }


}
