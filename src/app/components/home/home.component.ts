import { Component, OnInit } from '@angular/core';
import { HeroeComponent } from '../heroe/heroe.component';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _router: Router
  ) { }

  ngOnInit() {
  }

  heroe(){
    this._router.navigate(['/heroes']);
  }
}

