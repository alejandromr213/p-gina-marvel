import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {HeroesService, Heroe} from '../../servicios/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {
  
  public heroes:Heroe[] = [];
  heroe:any={};
  constructor(
    private activateRoute : ActivatedRoute,
    private _heroesService: HeroesService,
    private _router: Router
   ) { 

this.activateRoute.params.subscribe(
  params=>{
  this.heroe=this._heroesService.getHeroe(params['id']);

})




  }

  ngOnInit() {

    this.heroes = this._heroesService.getHeroes();

    // console.log(this.heroes);
  
  }

volver(){
  this._router.navigate(['/heroes']);
}

}
