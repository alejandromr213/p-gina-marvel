import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {HeroesService} from '../../servicios/heroes.service';

@Component({
  selector: 'app-heroe-busqueda',
  templateUrl: './heroe-busqueda.component.html',
  styleUrls: ['./heroe-busqueda.component.css']
})
export class HeroeBusquedaComponent implements OnInit {
  heroe:any = {};

  constructor(
   private activateRoute: ActivatedRoute,
   private _heroesService : HeroesService,
   private _router: Router
  ) { 
    this.activateRoute.params.subscribe( params =>{
      // console.log (params['id']);
      this.heroe = this._heroesService.getHeroeArr(params['id']);
      // console.log(this.heroe);
    });
  }
  

  ngOnInit() {
  //  console.log(this.heroe);
  }

  volver(){
    this._router.navigate(['heroes']);
  }

}
